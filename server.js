#!/bin/env node

var express = require('express');
var http = require('http');
var deployd = require('deployd');
var io = require('socket.io');

var Loq = function() {
    var self = this;
    // Set the environment variables
    self.setupVariables = function() {
        self.ipaddress = process.env.OPENSHIFT_NODEJS_IP || '127.0.0.1';
        self.port = process.env.OPENSHIFT_NODEJS_PORT || 8080;
        self.db = process.env.OPENSHIFT_MONGODB_DB_URL || 'mongodb://127.0.0.1:27017/' + process.env.OPENSHIFT_APP_NAME;
        self.env = 'development';
    };
    // Initialize the server
    self.initializeServer = function() {
        self.app = express();
        self.server = http.createServer(self.app);
        self.socketIo = io.listen(self.server);
    };
    // Initialize application
    self.initialize = function() {
        self.setupVariables();
        self.initializeServer();
    };
    // Start the server
    self.start = function() {
        deployd.attach(self.server, {
            socketIo: self.socketIo,
            port: self.port,
            env: self.env,
            db: {
                host: process.env.OPENSHIFT_MONGODB_DB_HOST,
                port: process.env.OPENSHIFT_MONGODB_DB_PORT,
                name: process.env.OPENSHIFT_APP_NAME,
                credentials: {
                    username: process.env.OPENSHIFT_MONGODB_DB_USERNAME,
                    password: process.env.OPENSHIFT_MONGODB_DB_PASSWORD
                }
            }
        });
        self.app.use(self.server.handleRequest);
        self.server.listen(self.port, self.ipaddress);
    };
};

// Main
var loq = new Loq();
loq.initialize();
loq.start();